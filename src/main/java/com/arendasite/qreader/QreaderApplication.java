package com.arendasite.qreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class QreaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(QreaderApplication.class, args);
    }

}
