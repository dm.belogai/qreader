package com.arendasite.qreader.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;



@Configuration
public class SecureConfig extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception{
            http.
                    csrf()
                    .disable()
                    .authorizeRequests()
                    .antMatchers("/login", "/images/**", "/resources/**", "/css/**", "/js/**").permitAll()
                    .antMatchers("/", "/logout", "/invitation/**", "/qrcode").hasRole("USER")
                    .antMatchers("/admin", "/registration/**", "/admin/**").hasRole("ADMIN")
                    .and()
                    .rememberMe()
                    .and()
                    .logout().logoutUrl("/logout").logoutSuccessUrl("/login").deleteCookies("remember-me")
                    .and()
                    .formLogin(form -> form.loginPage("/login"));

    }

    @Bean
    public PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
}
