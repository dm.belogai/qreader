package com.arendasite.qreader.controllers;
import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.QRData;
import com.arendasite.qreader.entities.Role;
import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.services.OrganizationService;
import com.arendasite.qreader.services.QRcodeServise;
import com.arendasite.qreader.services.RoleService;
import com.arendasite.qreader.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.http.HttpResponse;

@AllArgsConstructor
@Controller
public class AdminController {

UserService userService;
RoleService roleService;
QRcodeServise qRcodeServise;
OrganizationService organizationService;



    @GetMapping(value = {"/admin/{delete_attempt}", "/admin"})
    public String getPage(Model model, @PathVariable(required = false) String delete_attempt){

        model.addAttribute("userTable", userService.getOrderedUserList().orElseThrow(()
                -> new RuntimeException("Отсутствует список пользователей")));
        model.addAttribute("userForm", new User());


        model.addAttribute("orgTable", organizationService.getOrderedOrgList().orElseThrow(()
                -> new RuntimeException("Отсутствует список организаций")));
        model.addAttribute("orgForm", new Organization());


        model.addAttribute("qrTable", qRcodeServise.getOrderedQRList().orElseThrow(()
                -> new RuntimeException("Отсутствует список QR-кодов")));
        model.addAttribute("qrForm", new QRData());


        model.addAttribute("roleTable", roleService.getOrderedRoleList().orElseThrow(()
                -> new RuntimeException("Отсутствует список ролей")));
        model.addAttribute("roleForm", new Role());
        if(delete_attempt != null){
             if(delete_attempt.equals("deny")){
                 model.addAttribute("Error", "Удаление записи, нарушающей ограничение FK невозможно!\n" +
                    "Удалите все связанные поля из таблицы users_roles");
            }
        }

        return "admin";
    }

    @PostMapping("/admin/tables/delete")
    public String editDB(Model model,
                         @RequestParam(required = true, defaultValue = "" ) Long id,
                         @RequestParam(required = false, defaultValue = "" ) Long idUser,
                         @RequestParam(required = true, defaultValue = "" ) String action){

        if(action.equals("deleteUser")){
            userService.deleteUser(id);
        }
        if(action.equals("deleteRole")){
            try{
                roleService.deleteRole(id);
            }
            catch (Exception e) {
                return "redirect:/admin/deny";
            }
        }
        if(action.equals("deleteOrg")){
            organizationService.deleteOrg(id);
        }
        if(action.equals("deleteQR")){
            qRcodeServise.deleteQr(id);
        }
        if(action.equals("deleteUserRole")){
            userService.removeRole(userService.getUserById(idUser),
                    roleService.findRoleById(id));
        }
        return "redirect:/admin";
    }

}
