package com.arendasite.qreader.controllers;

import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.services.QRcodeServise;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class IndexController {

    private final QRcodeServise qrCodeServise;

    @GetMapping("/")
    public String getIndex(Model model){
        model.addAttribute("qrs",
                qrCodeServise.getQRListOfUser(
                        (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()
                )
        );
        return "index";
    }
}
