package com.arendasite.qreader.controllers;

import com.arendasite.qreader.entities.QRData;
import com.arendasite.qreader.enums.DocumentType;
import com.arendasite.qreader.enums.EnteringWay;
import com.arendasite.qreader.repositories.OrganizationRepository;
import com.arendasite.qreader.repositories.QrcodeRepository;
import com.arendasite.qreader.repositories.UserRepository;
import com.arendasite.qreader.services.InvitationService;
import com.arendasite.qreader.services.OrganizationService;
import com.arendasite.qreader.services.QRcodeServise;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;



@AllArgsConstructor
@Controller
public class InvitationController {

    OrganizationService organizationService;
    QRcodeServise qrService;
    InvitationService invitationService;
    SimpleDateFormat sdf;




    @GetMapping(value = {"/invitation/{qr_id}", "/invitation"})
    public String loadPage(Model model, @PathVariable(required = false) Long qr_id){

        var data = new QRData();
        model.addAttribute("documentTypes", DocumentType.values());
        model.addAttribute("enteringWays", EnteringWay.values());
        if(qr_id != null) {
            model.addAttribute("qr_id", qr_id);
        }
        organizationService.getOrderedOrgList().orElseThrow(() -> new RuntimeException("Отсутствует список организаций!"));
            model.addAttribute("organizationsList",
                    invitationService.fixedList(organizationService.getOrderedOrgList().get(),
                            (int) invitationService.getUser().getOrg().getId() - 1, 0));

        model.addAttribute("dataForm", data);
        return "invitation";
    }

    @PostMapping("/invitation")
    public String invite(Model model, @ModelAttribute("dataForm") @Valid QRData data, final BindingResult bindingResult) throws ParseException {

        if(bindingResult.hasErrors()){
            model.addAttribute("error", "Неверно указаны данные");
            return "invitation";
        }
        if(sdf.parse(data.getDate()).compareTo(sdf.parse(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))) < 0){
            model.addAttribute("error", "Неверно указана дата посещения");
            return "invitation";
        }

        qrService.saveQR(data);
        return "redirect:/invitation/" + data.getId();
    }
}
