package com.arendasite.qreader.controllers;

import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@AllArgsConstructor
@Controller
public class LoginController {

    PasswordEncoder encoder;
    UserRepository userRepository;

    @GetMapping("/login")
        public String getPage(Model model) {
        model.addAttribute("userForm", new User());
        return "login";
    }
    @PostMapping("/login")
    public String login(@ModelAttribute("userForm") User user, Model model) {
        if(userRepository.findByUsername(user.getUsername()).isPresent()){
            var userDetails = userRepository.findByUsername(user.getUsername()).get();
            if(encoder.matches(user.getPassword(),
                    userDetails.getPassword()) && user.getEmail().equals(userDetails.getEmail()))
            {
                 return "redirect:/index";
            }
        }
        model.addAttribute("error", "Неверный пароль или данные аккаунта");
        return  "login";

    }
}
