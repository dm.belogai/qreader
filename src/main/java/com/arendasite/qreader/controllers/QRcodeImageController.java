package com.arendasite.qreader.controllers;


import com.arendasite.qreader.repositories.QrcodeRepository;
import com.arendasite.qreader.services.QRcodeServise;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@AllArgsConstructor
public class QRcodeImageController {

    private final QRcodeServise qRcodeServise;
    private final QrcodeRepository qRcodeRepository;

    @GetMapping(value = "/{qr_id}/qrcode", produces = MediaType.IMAGE_JPEG_VALUE)
    public String showQRcode(@PathVariable Long qr_id, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        try {
          MatrixToImageWriter.writeToStream(qRcodeServise.generateQR(qRcodeRepository.getById(qr_id)), "PNG", response.getOutputStream());
        }
        catch (Exception e){
        }
        response.getOutputStream().flush();
        response.getOutputStream().close();
        return "qrcode";
    }
}

