package com.arendasite.qreader.controllers;

import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.QRData;
import com.arendasite.qreader.entities.Role;
import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@AllArgsConstructor
@Controller
public class RegistrationController
{

    private UserService userService;

        @GetMapping("/registration/{table}")
        public static String getPage(@PathVariable(name="table") String table, Model model)
        {

            model.addAttribute("entityName", table);
            switch (table) {
                case "user" -> model.addAttribute("userForm", new User());
                case "role" -> model.addAttribute("roleForm", new Role());
                case "org" -> model.addAttribute("orgForm", new Organization());
                default -> {
                }
            }

            return "registration";
        }
        @PostMapping("/registration")
        public String register(@ModelAttribute("userForm") User user, BindingResult bindingResult, Model model){

        if(bindingResult.hasErrors()){
            model.addAttribute("error", "Ошибка регистрации, проверьте вводимые данные");
            return "registration";
        }

        if(userService.saveNewUser(user)){
        return "redirect:/login";
        }
        return "registration";
    }
}
