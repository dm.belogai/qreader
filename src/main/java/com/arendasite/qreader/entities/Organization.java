package com.arendasite.qreader.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "organizations")
@Entity
@Getter
@Setter
public class Organization {
    public Organization() {
    }

    public Organization(int id, String orgName){
        this.id = id;
        this.orgName = orgName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private long id;

    @Column
    private String orgName;

    public long getId() {
        return id;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "org")
    private User user;

}
