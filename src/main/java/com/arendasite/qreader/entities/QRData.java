package com.arendasite.qreader.entities;


import com.arendasite.qreader.enums.DocumentType;
import com.arendasite.qreader.enums.EnteringWay;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "QRData")
public class QRData {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    @Column
    private EnteringWay enteringWay;
    @Column
    private DocumentType documentType;
    @Column
    private String surname;

    public void setSurname(String surname) {
        surname.toLowerCase();
        StringUtils.capitalize(surname);
        this.surname = surname;
    }

    public void setName(String name) {
        name.toLowerCase();
        StringUtils.capitalize(name);
        this.name = name;
    }

    public void setPatronymic(String patronymic) {
        patronymic.toLowerCase();
        StringUtils.capitalize(patronymic);
        this.patronymic = patronymic;
    }

    @Column
    private String name;
    @Column
    private String patronymic;
    @Column
    private String idSeries;
    @Column
    private String idNumber;



    @Column
    private String date;

    @Column
    private Long organizationId;

    public QRData() {
    }

    public String ToString(){
        String data = this.date.toString() + "\n";
        data+= this.surname                + "\n";
        data+= this.name                   + "\n";
        data+= this.patronymic             + "\n";
        data+= this.idSeries               + "\n";
        data+= this.idNumber               + "\n";
        data+= this.organizationId         + "\n";
        data+= this.id;
        return data;
    }
}
