package com.arendasite.qreader.entities;


import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Table(name = "roles")
@Entity
public class Role implements GrantedAuthority {
    public Role() {
    }

    public Role(Long id, String roleName){
        this.id = id;
        this.roleName = roleName;
    }

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String roleName;


    @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
    private Collection<User> users = new ArrayList<User>();


    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public long getId() {
        return id;
    }

    public String getRoleName() {
        return roleName;
    }

    @Override
    public String getAuthority() {
        return this.roleName;
    }
}
