package com.arendasite.qreader.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DocumentType {
    NATIVE_ID("Паспорт гражданина РФ"),
    FOREIGN_ID("Загран паспорт"),
    DRIVER_LICENSE("Водительское удостоверение");



    private String code;
}
