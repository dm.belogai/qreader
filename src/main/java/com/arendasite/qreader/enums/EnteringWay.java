package com.arendasite.qreader.enums;

import lombok.AllArgsConstructor;

public enum EnteringWay {
    BY_FEET("Пешком"),
    ON_CAR("На машине");

    private String code;

    EnteringWay(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }



    public static EnteringWay fromCode(String way) {
        for (EnteringWay eWay : EnteringWay.values()) {
            if (eWay.getCode().equals(way)) {
                return eWay;
            }
        }
        throw new UnsupportedOperationException("The code " + way + " is not supported!");
    }
}
