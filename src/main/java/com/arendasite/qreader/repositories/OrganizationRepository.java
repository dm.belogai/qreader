package com.arendasite.qreader.repositories;

import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {
    public Optional<List<Organization>> findAllByOrderByIdAsc();
    public Optional<Organization> findById(Long id);
}
