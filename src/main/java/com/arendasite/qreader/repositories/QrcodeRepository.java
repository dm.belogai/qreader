package com.arendasite.qreader.repositories;

import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.QRData;
import com.arendasite.qreader.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface QrcodeRepository extends JpaRepository<QRData, Long> {
    public Optional<List<QRData>> findAllByOrderByIdAsc();
    public Optional<QRData> findById(Long id);

    @Query(value = "select * from QRData q where q.organizationId = :orgId",
    nativeQuery = true)
    public Optional<List<QRData>> findAllByUserOrgId(@Param("orgId") Long orgId);
}
