package com.arendasite.qreader.repositories;

import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.Role;
import com.arendasite.qreader.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRoleName(String roleName);
    @Query("select r from Role r join User u where u.username = :username")
    List<Role> getRoleOfUser(@Param("username") String username);
    public Optional<List<Role>> findAllByOrderByIdAsc();
    public Optional<Role> findById(Long id);
}
