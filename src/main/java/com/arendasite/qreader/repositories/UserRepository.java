package com.arendasite.qreader.repositories;

import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> findByUsername(String username);
    public Optional<User> findByEmail(String email);
    public Optional<List<User>> findAllByOrderByIdAsc();
    public Optional<User> findById(Long id);
}
