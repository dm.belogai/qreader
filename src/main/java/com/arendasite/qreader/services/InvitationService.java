package com.arendasite.qreader.services;


import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.repositories.OrganizationRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class InvitationService {

    OrganizationRepository organizationRepository;


    public List<Organization> fixedList(List<Organization> all, int swapThis, int swapTo){
        var allOrgs = all;
        Collections.swap(
                allOrgs,
                swapThis,
                swapTo);
        return allOrgs;
    }


    public User getUser(){
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
