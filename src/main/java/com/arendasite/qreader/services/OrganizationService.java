package com.arendasite.qreader.services;


import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.exceptions.NotFoundException;
import com.arendasite.qreader.repositories.OrganizationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class OrganizationService {
    private final OrganizationRepository organizationRepository;

    public Optional<List<Organization>> getOrderedOrgList(){
       return organizationRepository.findAllByOrderByIdAsc();
    }
    public void deleteOrg(Long id)throws RuntimeException{
        organizationRepository.findById(id).orElseThrow(()
                -> new NotFoundException("Не удалось найти арендатора с таким ID!: " + id));
        organizationRepository.deleteById(id);
    }
}
