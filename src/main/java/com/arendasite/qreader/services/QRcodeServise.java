package com.arendasite.qreader.services;

import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.entities.QRData;
import com.arendasite.qreader.entities.Role;
import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.exceptions.NotFoundException;
import com.arendasite.qreader.repositories.QrcodeRepository;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class QRcodeServise {

    private final QrcodeRepository qrcodeRepository;


    public BitMatrix generateQR(QRData qrData) throws WriterException {
        return new MultiFormatWriter().encode(
                    new String(qrData.toString().getBytes(StandardCharsets.UTF_8),
                StandardCharsets.UTF_8),
                BarcodeFormat.QR_CODE,
                200,
                200);
    }
    public void saveQR(QRData qrData){
        qrcodeRepository.save(qrData);
    }

    public Optional<List<QRData>> getOrderedQRList(){
        return   qrcodeRepository.findAllByOrderByIdAsc();
    }

    public void deleteQr(Long id) throws NotFoundException{
        qrcodeRepository.findById(id).orElseThrow(()
                -> new NotFoundException("Не удалось найти QR-код с таким ID!: " + id));
        qrcodeRepository.deleteById(id);
    }

    public List<QRData> getQRListOfUser(User user){
       return qrcodeRepository.findAllByUserOrgId(user.getOrg().getId()).orElseThrow(()
               ->new NotFoundException("Список пользователя пуст или пользователя" +
               " с Id: " + user.getId() + " не существует!"));
    }

}
