package com.arendasite.qreader.services;

import com.arendasite.qreader.entities.Role;
import com.arendasite.qreader.exceptions.NotFoundException;
import com.arendasite.qreader.repositories.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    public Optional<List<Role>> getOrderedRoleList(){
      return   roleRepository.findAllByOrderByIdAsc();
    }
    public void deleteRole(Long id)throws RuntimeException{
        roleRepository.findById(id).orElseThrow(()
                -> new NotFoundException("Не удалось найти роль с таким ID!: " + id));
        roleRepository.deleteById(id);
    }

    public Role findRoleById(Long id){
       return roleRepository.findById(id).orElseThrow(()
                -> new NotFoundException("Не удалось найти роль с таким ID!: " + id));
    }
}
