package com.arendasite.qreader.services;

import com.arendasite.qreader.entities.Role;
import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.exceptions.NotFoundException;
import com.arendasite.qreader.repositories.RoleRepository;
import com.arendasite.qreader.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(()->new BadCredentialsException("user not found"));
    }



    public Boolean saveNewUser(User newUser) throws  BadCredentialsException{
        if (userRepository.findByUsername((newUser.getUsername())).isPresent()){
            throw  new BadCredentialsException("user already exists");
        }

        else {
            newUser.setEnabled(true);
            newUser.setCredentialsNonExpired(true);
            newUser.setAccountNonLocked(true);
            newUser.setAccountNonExpired(true);
            addRole(newUser, roleRepository.findByRoleName("ROLE_USER"));
            userRepository.save(newUser);
            return true;
        }
    }

    public Optional<List<User>> getOrderedUserList(){
        return   userRepository.findAllByOrderByIdAsc();
    }

    public void deleteUser(Long id)throws RuntimeException{
        userRepository.findById(id).orElseThrow(()
                -> new NotFoundException("Не удалось найти пользователя с таким ID!: " + id));
        userRepository.deleteById(id);
    }


    public void addRole(User user, Role role){
        List<Role> newList = new ArrayList<>(user.getRoles());
        newList.add(role);
        user.setRoles(newList);
        userRepository.save(user);
    }

    public void removeRole(User user, Role role){
        List<Role> newList = new ArrayList<>(user.getRoles());
        newList.remove(role);
        user.setRoles(newList);
        userRepository.save(user);
    }


    public User getUserById(Long id){
       return userRepository.findById(id).orElseThrow(()
                -> new NotFoundException("Не удалось найти пользователя с таким ID!: " + id));
    }
}
