function hide_users()
{
    if (document.getElementById("displaytableusers").style.display === "none") {
        document.getElementById("displaytableusers").style.display = "block";
        document.getElementById("displaytableroles").style.display = "none";
        document.getElementById("displaytableorgs").style.display="none";
        document.getElementById("displaytableqrs").style.display="none";
        document.getElementById("displaytableusersroles").style.display="none";
    }
    else
        document.getElementById("displaytableusers").style.display="none";
}


function hide_roles()
{
    if (document.getElementById("displaytableroles").style.display === "none") {
        document.getElementById("displaytableroles").style.display = "block";
        document.getElementById("displaytableusers").style.display = "none";
        document.getElementById("displaytableorgs").style.display="none";
        document.getElementById("displaytableqrs").style.display="none";
        document.getElementById("displaytableusersroles").style.display="none";
    }
    else
        document.getElementById("displaytableroles").style.display="none";
}


function hide_orgs()
{
    if (document.getElementById("displaytableorgs").style.display === "none") {
        document.getElementById("displaytableorgs").style.display = "block";
        document.getElementById("displaytableroles").style.display = "none";
        document.getElementById("displaytableusers").style.display="none";
        document.getElementById("displaytableqrs").style.display="none";
        document.getElementById("displaytableusersroles").style.display="none";
    }
    else
        document.getElementById("displaytableorgs").style.display="none";
}

function hide_qrs()
{
    if (document.getElementById("displaytableqrs").style.display === "none") {
        document.getElementById("displaytableqrs").style.display = "block";
        document.getElementById("displaytableroles").style.display = "none";
        document.getElementById("displaytableorgs").style.display="none";
        document.getElementById("displaytableusers").style.display="none";
        document.getElementById("displaytableusersroles").style.display="none";
    }
    else
        document.getElementById("displaytableqrs").style.display="none";
}

function hide_user_roles(){
    if (document.getElementById("displaytableusersroles").style.display === "none") {
        document.getElementById("displaytableusersroles").style.display = "block";
        document.getElementById("displaytableroles").style.display = "none";
        document.getElementById("displaytableorgs").style.display="none";
        document.getElementById("displaytableusers").style.display="none";
        document.getElementById("displaytableqrs").style.display="none";
    }
    else
        document.getElementById("displaytableusersroles").style.display="none";
}