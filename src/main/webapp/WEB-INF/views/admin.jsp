<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Администрирование</title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/page_mask.css' />"  />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/admin_page.css' />"  />
    <script src="<c:url value='/js/hide-on-click.js' />" defer></script>
</head>


<body>

<div class="header">
    <div class="left-div">
        <a href="/" class="index">Главная</a>
    </div>
    <div class="mid-div">
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="/admin" class="middle selected">Администрирование</a>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_USER')">
            <a href="/invitation" class="middle">Приглашения</a>
        </sec:authorize>
    </div>
    <div class="right-div">
        <sec:authorize access="isAuthenticated()">
            <a class = "logout" href="/logout">Выйти</a>
        </sec:authorize>
    </div>
</div>
<div style="text-align: center"  class="content">
    <h2>${Error}</h2>
    <div class="buttons">
        <button type="button" onclick="hide_users()">Пользователи</button>
        <button type="button" onclick="hide_roles()">Роли</button>
        <button type="button" onclick="hide_qrs()">QR-коды</button>
        <button type="button" onclick="hide_orgs()">Организации</button>
        <button type="button" onclick="hide_user_roles()">Пользователи и их Роли</button>
    </div>


    <div style="display: none" id="displaytableusers">
    <table id="tbluser">
        <thead>
        <tr>
            <th>Id</th>
            <th>Логин</th>
            <th>Email</th>
            <th>Организация</th>
        </tr>
        </thead>
        <tbody>
        <c:choose>
            <c:when test="${userTable.isEmpty()}">
                <tr>
                    <td colspan="2">Отсутствуют элементы отвечающие запросу</td>
                </tr>
            </c:when>
            <c:otherwise>
                <c:forEach items="${userTable}" var="user">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.username}</td>
                        <td>${user.email}</td>
                        <td>${user.org.orgName}</td>
                        <td class="non-border">
                            <form action="/admin/tables/delete" method="post">
                                <input type="hidden" name="id" value="${user.id}"/>
                                <input type="hidden" name="action" value="deleteUser"/>
                                <button type="submit">Удалить</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </c:otherwise>
        </c:choose>
        <tr>
            <td class="last add">
                <a href="/registration/user">Создать</a>
            </td>
        </tr>
        </tbody>
    </table>
    </div>

    <div style="display: none" id="displaytableusersroles">
        <table id="tblusersroles">
            <thead>
            <tr>
                <th>Id Пользователя</th>
                <th>Название Роли</th>
            </tr>
            </thead>
            <tbody>
            <c:choose>
                <c:when test="${userTable.isEmpty()}">
                    <tr>
                        <td colspan="2">Отсутствуют элементы отвечающие запросу</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${userTable}" var="user">
                            <c:forEach items="${user.roles}" var="role">
                                <tr>
                                <td>${user.id}</td>
                                <td>${role.roleName}</td>
                            <td class="non-border">
                                <form action="/admin/tables/delete" method="post">
                                    <input type="hidden" name="id" value="${role.id}"/>
                                    <input type="hidden" name="idUser" value="${user.id}"/>
                                    <input type="hidden" name="action" value="deleteUserRole"/>
                                    <button type="submit">Удалить</button>
                                </form>
                            </td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            <tr>
                <td class="last add">
                    <a href="/registration/userRole">Создать</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div style="display: none" id="displaytableroles">
        <table id="tblrole">
            <thead>
            <tr>
                <th>Id</th>
                <th>Название роли</th>
            </tr>
            </thead>
            <tbody>
            <c:choose>
                <c:when test="${roleTable.isEmpty()}">
                    <tr>
                        <td colspan="2">Отсутствуют элементы отвечающие запросу</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${roleTable}" var="role">
                        <tr>
                            <td>${role.id}</td>
                            <td>${role.roleName}</td>
                            <td class="non-border">
                                <form action="/admin/tables/delete" method="post">
                                    <input type="hidden" name="id" value="${role.id}"/>
                                    <input type="hidden" name="action" value="deleteRole"/>
                                    <button type="submit">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            <tr>
                <td class="last add">
                    <a href="/registration/role">Создать</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


    <div style="display: none" id="displaytableorgs">
        <table id="tblorg">
            <thead>
            <tr>
                <th>Id</th>
                <th>Название Организации</th>
            </tr>
            </thead>
            <tbody>
            <c:choose>
                <c:when test="${orgTable.isEmpty()}">
                    <tr>
                        <td colspan="2">Отсутствуют элементы отвечающие запросу</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${orgTable}" var="org">
                        <tr>
                            <td>${org.id}</td>
                            <td>${org.orgName}</td>
                            <td class="non-border">
                            <form action="/admin/tables/delete" method="post">
                                <input type="hidden" name="id" value="${org.id}"/>
                                <input type="hidden" name="action" value="deleteOrg"/>
                                <button type="submit">Удалить</button>
                            </form>
                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            <tr>
                <td class="last add">
                    <a href="/registration/org">Создать</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


    <div style="display: none" id="displaytableqrs">
        <table id="tblqr">
            <thead>
            <tr>
                <th>Id</th>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>Тип документа удостоверяющего личность</th>
                <th>Серия</th>
                <th>Номер</th>
                <th>Дата прохода</th>
                <th>Организация-пригласитель</th>
            </tr>
            </thead>
            <tbody>
            <c:choose>
                <c:when test="${qrTable.isEmpty()}">
                    <tr>
                        <td colspan="2">Отсутствуют элементы отвечающие запросу</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${qrTable}" var="qr">
                        <tr>
                            <td>${qr.id}</td>
                            <td>${qr.surname}</td>
                            <td>${qr.name}</td>
                            <td>${qr.patronymic}</td>
                            <td>${qr.documentType}</td>
                            <td>${qr.idSeries}</td>
                            <td>${qr.idNumber}</td>
                            <td>${qr.date}</td>
                            <td>${qr.organizationId}</td>
                            <td class="non-border">
                                <form action="/admin/tables/delete" method="post">
                                    <input type="hidden" name="id" value="${qr.id}"/>
                                    <input type="hidden" name="action" value="deleteQR"/>
                                    <button type="submit">Удалить</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            <tr>
            <td class="last add">
                <a href="/invitation">Создать</a>
            </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>