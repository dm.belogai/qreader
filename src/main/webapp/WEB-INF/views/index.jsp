<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Главная страница</title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/page_mask.css' />"  />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/index_page.css' />"  />
</head>


<body>

<div class="header">
    <div class="left-div">
        <a href="/" class="index selected">Главная</a>
    </div>
    <div class="mid-div">
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="/admin" class="middle">Администрирование</a>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_USER')">
            <a href="/invitation" class="middle">Приглашения</a>
        </sec:authorize>
    </div>
    <div class="right-div">
        <sec:authorize access="isAuthenticated()">
            <a class = "logout" href="/logout">Выйти</a>
        </sec:authorize>
    </div>
</div>
<div style="text-align: center" class="content">
    <table>
        <thead>
        <tr>
            <th>ФИО посетитиля</th>
            <th>Дата прохода</th>
            <th>Просрочен</th>
            <th>QR-код</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${qrs}" var="qr">
            <tr>
                <td class="wid30">${qr.surname}  ${qr.name}  ${qr.patronymic}</td>
                <td class="wid20">${qr.date}</td>
                <td class="wid5"><p id="expired">НЕТ</p></td>
                <td class="non-border wid20"><img src="/${qr.id}/qrcode" alt="ᅠ ᅠ " width="150" height="150"/></td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
</div>

</body>


</html>