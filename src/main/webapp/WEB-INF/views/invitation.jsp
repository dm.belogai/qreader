<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <title>Создание приглашений</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/invitation_page.css' />"  />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/page_mask.css' />"  />
    <script
            src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous"></script>
    <script src="<c:url value='/js/mask.js' />" defer></script>
    <script src="<c:url value='/js/jquery.maskedinput.js' />" defer></script>
</head>

<body>
<div class="header">
    <div class="left-div">
        <a href="/" class="index">Главная</a>
    </div>
    <div class="mid-div">
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="/admin" class="middle">Администрирование</a>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_USER')">
            <a href="/invitation" class="middle selected">Приглашения</a>
        </sec:authorize>
    </div>
    <div class="right-div">
        <sec:authorize access="isAuthenticated()">
            <a class = "logout" href="/logout">Выйти</a>
        </sec:authorize>
    </div>
</div>
<div class="header-mobile">
    <button class="dropbtn"><i class="fa fa-bars"></i></button>
     <div class="dropdown-content">
        <a href="/">Главная</a>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="/admin">Администрирование</a>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_USER')">
            <a href="/invitation/0">Приглашения</a>
        </sec:authorize>
    </div>
    <sec:authorize access="isAuthenticated()">
        <a href="/logout" class="logout">Выйти</a>
    </sec:authorize>
</div>
<div class ="content">
    <div style="text-align: center;">
        <form:form action="/invitation" modelAttribute="dataForm" method="POST">
        <div class="container">
            <h1>Регистрация посетителя</h1>
            <div class="blank">
                <div class="bio">
                    <form:input type="text"
                                placeholder="Фамилия"
                                name="id_surname"
                                path="surname"
                                maxlength='64'
                                autofocus="autofocus"
                                required="required" />
                    <form:input type="text"
                                placeholder="Имя"
                                name="id_name"
                                path="name"
                                maxlength='64'
                                autofocus="autofocus"
                                required="required" />
                    <form:input type="text"
                                placeholder="Отчество (если имеетсся)"
                                name="id_patronymic"
                                path="patronymic"
                                maxlength='64'
                                autofocus="autofocus" />
                </div>
                <div class="id_series&number">
                    <form:select multiple="false" class = "dropdown" id="selector_papers" path="documentType" required="required">
                        <form:option disabled="" selected="" value="">Тип документа, удостоверяющего личность</form:option>
                        <form:options items="${documentTypes}" itemLabel="code"  />
                    </form:select>
                    <form:input type="text" placeholder="Серия"
                                name="id_series_field"
                                path="idSeries"
                                id = "series"
                                maxlength="2"
                                minlenght="2"
                                autofocus="autofocus"
                                required="required" />
                    <form:input type="text"
                                id = "number"
                                placeholder="Номер"
                                name="id_number_field"
                                path="idNumber"
                                maxlength="4"
                                minlenght="4"
                                autofocus="autofocus"
                                required="required" />
                </div>
                <div class="enter&org">
                    <form:select path="enteringWay" multiple="false" required="required">
                        <form:option value="" selected="" disabled="">Способ прохода на территорию</form:option>
                        <form:options items="${enteringWays}" itemLabel="code"  />
                    </form:select>
                    <form:select multiple="false" class="dropdown" path="organizationId" id="Org" required="required">
                        <form:option value="" selected="" disabled="">Организация-пригласитель</form:option>
                        <c:forEach items="${organizationsList}" var="org">
                            <form:option value="${org.id}">${org.orgName}</form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
            <div class="calendar">
                <label><b>Дата посещения</b></label>
                <div class="date">
                    <form:input type="date"
                                placeholder="Дата посещения"
                                name="visit_date"
                                path="date"
                                maxlength='64'
                                autofocus="autofocus"
                                required="required" />
                </div>
            </div>
            <hr />
            <h1>${error}</h1>
            <button type="submit" class="confirm">Создать приглашение</button>
            </form:form>
            <hr />

            <img src="/${qr_id}/qrcode" alt="ᅠ ᅠ " width="200" height="200"/>

        </div>
    </div>
</div>
</body>
</html>