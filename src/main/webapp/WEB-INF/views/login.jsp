<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Аутентификация</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/login_page.css' />"  />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/media_shrink.css' />"  />
</head>

<body>
<div style="text-align: center;">
    <div class="content">
        <div class="blank">
    <form:form action="/login" modelAttribute="userForm" method="POST">
            <h1>Вход</h1>
            <img src="images/blank-user-image.png" alt="blank-image" width="150" height="150">
        <hr/>
        <div class="inputs">
            <form:input type="text" placeholder="Имя пользователя"  path="username" name="username_field" maxlength='32' autofocus="autofocus" required="required" />

            <form:input type="email" placeholder="Email"  path="email" name="email_field" maxlength='32' autofocus="autofocus" required="required" />

            <form:input type="password" placeholder="Пароль" path="password" name="pass_field" maxlength='32' required="required" />
        </div>

            <button type="submit" class="login">Войти</button>
        <hr />
        <h1>${error}</h1>
    </form:form>
        </div>
    </div>
</div>
</body>
</html>