<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>QR код</title>

</head>

<body>
<div style="text-align: center;">
    <img src="${"/images/QRcode.jpg"}" alt="QR CODE IMAGE" width="100" height="100"/>
</div>
</body>
</html>