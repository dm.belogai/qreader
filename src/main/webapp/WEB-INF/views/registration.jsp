<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Аутентификация</title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/registration_page.css' />"  />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/media_shrink.css' />"  />

</head>

<body>
<div style="text-align: center;">
    <div class="content">
        <div class="blank">
            <h1>Создание Записи</h1>
            <c:choose>

                <c:when test="${entityName.equals('user')}">
                    <form:form action="/registration/${entityName}" modelAttribute="userForm" method="POST">
                <div class="inputs">
                    <form:input id="T" type="text" placeholder="Логин" name="username_field" path="username" maxlength='32' autofocus="autofocus" required="required" />
                    <form:input id="E" type="email" placeholder="Email" name="email_field" path="email" maxlength='32' autofocus="autofocus" required="required" />
                    <form:input id="P1" type="password" placeholder="Пароль"  name="pass_field" path="password" maxlength='32' required="required" />
                </div>
                        <button type="submit" class="confirm">Подтвердить</button>
                    </form:form>
                </c:when>


                <c:when test="${entityName.equals('role')}">
                <form:form action="/registration/${entityName}" modelAttribute="roleForm" method="POST">
                    <div class="inputs">
                        <form:input id="TRole" type="text" placeholder="Название роли" name="rolename_field" path="roleName" maxlength='32' autofocus="autofocus" required="required" />
                    </div>
                    <button type="submit" class="confirm">Подтвердить</button>
                </form:form>
                </c:when>


                <c:when test="${entityName.equals('org')}">
                    <form:form action="/registration/${entityName}" modelAttribute="orgForm" method="POST">
                        <div class="inputs">
                            <form:input id="TOrg" type="text" placeholder="Название организации" name="orgname_field" path="orgName" maxlength='32' autofocus="autofocus" required="required" />
                        </div>
                        <button type="submit" class="confirm">Подтвердить</button>
                    </form:form>
                </c:when>


                <c:when test="${entityName.equals('userRole')}">
                    <form action="/registration/${entityName}" modelAttribute="roleUserForm" method="POST">
                        <div class="inputs">
                            <input id="userId" type="number" placeholder="Id пользователя" name="userid_field"  maxlength='32' autofocus="autofocus" required="required" />
                            <input id="roleId" type="number" placeholder="Id роли" name="roleid_field"  maxlength='32' autofocus="autofocus" required="required" />
                        </div>
                        <button type="submit" class="confirm">Подтвердить</button>
                    </form>
                </c:when>


                <c:otherwise>
                </c:otherwise>

            </c:choose>
            <h1>${error}</h1>
        </div>
    </div>
</div>
</body>
</html>