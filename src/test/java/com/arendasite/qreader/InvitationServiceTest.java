package com.arendasite.qreader;

import com.arendasite.qreader.entities.Organization;
import com.arendasite.qreader.repositories.OrganizationRepository;
import com.arendasite.qreader.services.InvitationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class InvitationServiceTest {

    private InvitationService toTestService;

    @Mock
    private OrganizationRepository organizationRepository;

    @Before
    public void setUp() {
        toTestService  = new InvitationService(organizationRepository);

    }

    @Test
    public void testFixedListMethod() {

        var a =    new Organization(1, "testOrg1");
        var b =    new Organization(2, "testOrg2");
        var c =    new Organization(3, "testOrg3");
        var d =    new Organization(4, "testOrg4");

        var l = toTestService.fixedList(
                new ArrayList<Organization>
                        (
                                Arrays.asList(a, b, c, d)
                        ), 2, 0);
        var r =  new ArrayList<Organization>
                (
                        Arrays.asList(c, b, a, d)
                );

           assertEquals(l, r);

    }
}
