package com.arendasite.qreader;


import com.arendasite.qreader.entities.Role;
import com.arendasite.qreader.entities.User;
import com.arendasite.qreader.repositories.OrganizationRepository;
import com.arendasite.qreader.repositories.RoleRepository;
import com.arendasite.qreader.repositories.UserRepository;
import com.arendasite.qreader.services.InvitationService;
import com.arendasite.qreader.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {


    private UserService toTestService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Before
    public void setUp() {
        toTestService  = new UserService(userRepository, roleRepository);
    }

    @Test
    public void itAddsRole(){
        var testUser = new User();
        var role1 = new Role(1L, "ROLE_TEST1");
        var role2 = new Role(2L, "ROLE_TEST2");
        testUser.setRoles(Arrays.asList(role1));

        toTestService.addRole(testUser, role2);
        var actual  = testUser.getRoles();
        var expected = Arrays.asList(role1, role2);
        assertEquals(actual, expected);
    }

    @Test
    public void itRemovesRole(){
        var testUser = new User();
        var role1 = new Role(1L, "ROLE_TEST1");
        var role2 = new Role(2L, "ROLE_TEST2");
        testUser.setRoles(Arrays.asList(role1, role2));

        toTestService.removeRole(testUser, role1);
        var actual  = testUser.getRoles();
        var expected = Arrays.asList(role2);
        assertEquals(actual, expected);
    }
}
